package Controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

@ManagedBean(name = "bean")
@SessionScoped
public class Fasulye {
   String sayfa;
  private String mesaj;
    public void idAl(ActionEvent event)
    {
        sayfa= event.getComponent().getId()+"?faces-redirect=true";
    }
    
    public void idAlQuery(ActionEvent event)
    {
        
        sayfa= "kategoriGoruntule?KategoriID="+event.getComponent().getAttributes().get("kategori")+"&satilik="+event.getComponent().getAttributes().get("satilikmi")+"&faces-redirect=true";
    }
    
    public String sayfaGit()
    {   
        return sayfa;
    }
    
    public void mesajGonder()
    {
        mesaj= "";
    }

    public String getMesaj() {
        return mesaj;
    }

    public void setMesaj(String mesaj) {
        this.mesaj = mesaj;
    }
    
    
}