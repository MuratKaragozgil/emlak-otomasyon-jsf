package Veritabani;

import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author OmerFaruk
 */

@ApplicationScoped
public class Oturum {
    private String kAdi,kSifre,kId;

    public String getkAdi() {
        return kAdi;
    }

    public void setkAdi(String kAdi) {
        this.kAdi = kAdi;
    }

    public String getkSifre() {
        return kSifre;
    }

    public void setkSifre(String kSifre) {
        this.kSifre = kSifre;
    }

    public String getkId() {
        return kId;
    }

    public void setkId(String kId) {
        this.kId = kId;
    }
    
    
    
    
    
    
    
}
