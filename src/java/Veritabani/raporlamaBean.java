package Veritabani;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Murat
 */
@ManagedBean
@RequestScoped
public class raporlamaBean {

    
       
        Connection con;
        private ResultSet rs=null;
        PreparedStatement SQLKomut;
        private int toplamMulkFiyatları,enUcuz,enPahalı,toplamMulkSayısı,toplamArsaSayısı;
        private int toplamVillaSayısı,toplamFabrikaSayısı,toplamDukkanSayısı,toplamDaireSayısı,toplamKayıtlıKullanıcı;
        private int kullaniciMaxMulkEkleme;
        private String maxMulkEkleyenKullanici;
        private Date enSonEklenme;
        
        public void mulkRaporlama() throws Exception
        {
            
        
                con = Database.getConnection();
                SQLKomut = con.prepareStatement("select fiyat from arsadetaylar \n" +
"union\n" +
"select fiyat from dairedetaylar\n" +
"union\n" +
"select fiyat from dukkandetaylar\n" +
"union\n" +
"select fiyat  from fabrikadetaylar\n" +
"union\n" +
"select fiyat from villadetaylar\n" +
"order by fiyat desc\n" +
"limit 1");
                rs=SQLKomut.executeQuery();
                rs.next();
              enPahalı=rs.getInt(1);
                
              SQLKomut = con.prepareStatement("select fiyat from arsadetaylar \n" +
"union\n" +
"select fiyat from dairedetaylar\n" +
"union\n" +
"select fiyat from dukkandetaylar\n" +
"union\n" +
"select fiyat  from fabrikadetaylar\n" +
"union\n" +
"select fiyat from villadetaylar\n" +
"order by fiyat\n" +
"limit 1");
               rs=SQLKomut.executeQuery();
               rs.next();
               enUcuz=rs.getInt(1);
               
                SQLKomut = con.prepareStatement("select Sum(fiyat) from arsadetaylar \n" +
"union\n" +
"select Sum(fiyat) from dairedetaylar\n" +
"union\n" +
"select Sum(fiyat) from dukkandetaylar\n" +
"union\n" +
"select Sum(fiyat) from fabrikadetaylar\n" +
"union\n" +
"select Sum(fiyat) from villadetaylar");
                
                rs=SQLKomut.executeQuery();
                
                toplamMulkFiyatları=0;
               while(rs.next())
                toplamMulkFiyatları+=rs.getInt(1);
               
               SQLKomut = con.prepareStatement("select kayittarihi from mulk\n" +
"order by kayittarihi desc");
                 rs=SQLKomut.executeQuery();
                 rs.next();
               enSonEklenme=rs.getDate(1);
               
                SQLKomut = con.prepareStatement("select Count(ilanid) from mulk");
                 rs=SQLKomut.executeQuery();
                 rs.next();
               toplamMulkSayısı=rs.getInt(1);
               
               SQLKomut = con.prepareStatement("select Count(ilanid) from mulk\n" +
"where kategoriID=0");
                 rs=SQLKomut.executeQuery();
                 rs.next();
               toplamArsaSayısı=rs.getInt(1);
               SQLKomut = con.prepareStatement("select Count(ilanid) from mulk\n" +
"where kategoriID=1");
                 rs=SQLKomut.executeQuery();
                 rs.next();
               toplamDaireSayısı=rs.getInt(1);
               SQLKomut = con.prepareStatement("select Count(ilanid) from mulk\n" +
"where kategoriID=2");
                 rs=SQLKomut.executeQuery();
                 rs.next();
               toplamDukkanSayısı=rs.getInt(1);
               SQLKomut = con.prepareStatement("select Count(ilanid) from mulk\n" +
"where kategoriID=3");
                 rs=SQLKomut.executeQuery();
                 rs.next();
               toplamFabrikaSayısı=rs.getInt(1);
               SQLKomut = con.prepareStatement("select Count(ilanid) from mulk\n" +
"where kategoriID=4");
                 rs=SQLKomut.executeQuery();
                 rs.next();
               toplamVillaSayısı=rs.getInt(1);
               rs.next();
               SQLKomut = con.prepareStatement(" select Count(id) from kullanicihesabi");
                 rs=SQLKomut.executeQuery();
                 rs.next();
               toplamKayıtlıKullanıcı=rs.getInt(1);
               
               SQLKomut = con.prepareStatement("SELECT kullanicihesabi.KullaniciAdi,COUNT(mulk.kimEklemis ) as kayitSayisi\n" +
"FROM kullanicihesabi\n" +
"LEFT JOIN mulk\n" +
"ON mulk.kimEklemis = kullanicihesabi.ID\n" +
"GROUP BY kullaniciAdi\n" +
"order by kayitSayisi desc limit 1");
               
               rs = SQLKomut.executeQuery();
               rs.next();
               
               maxMulkEkleyenKullanici = rs.getString(1);
               kullaniciMaxMulkEkleme = rs.getInt(2);
               
               con.close();

      
}

    public int getToplamMulkFiyatları() {
        return toplamMulkFiyatları;
    }

    public void setToplamMulkFiyatları(int toplamMulkFiyatları) {
        this.toplamMulkFiyatları = toplamMulkFiyatları;
    }

    public int getEnUcuz() {
        return enUcuz;
    }

    public void setEnUcuz(int enUcuz) {
        this.enUcuz = enUcuz;
    }

    public int getEnPahalı() {
        return enPahalı;
    }

    public void setEnPahalı(int enPahalı) {
        this.enPahalı = enPahalı;
    }

    public int getToplamMulkSayısı() {
        return toplamMulkSayısı;
    }

    public void setToplamMulkSayısı(int toplamMulkSayısı) {
        this.toplamMulkSayısı = toplamMulkSayısı;
    }

    public int getToplamArsaSayısı() {
        return toplamArsaSayısı;
    }

    public void setToplamArsaSayısı(int toplamArsaSayısı) {
        this.toplamArsaSayısı = toplamArsaSayısı;
    }

    public int getToplamVillaSayısı() {
        return toplamVillaSayısı;
    }

    public void setToplamVillaSayısı(int toplamVillaSayısı) {
        this.toplamVillaSayısı = toplamVillaSayısı;
    }

    public int getToplamFabrikaSayısı() {
        return toplamFabrikaSayısı;
    }

    public void setToplamFabrikaSayısı(int toplamFabrikaSayısı) {
        this.toplamFabrikaSayısı = toplamFabrikaSayısı;
    }

    public int getToplamDukkanSayısı() {
        return toplamDukkanSayısı;
    }

    public void setToplamDukkanSayısı(int toplamDukkanSayısı) {
        this.toplamDukkanSayısı = toplamDukkanSayısı;
    }

    public int getToplamDaireSayısı() {
        return toplamDaireSayısı;
    }

    public void setToplamDaireSayısı(int toplamDaireSayısı) {
        this.toplamDaireSayısı = toplamDaireSayısı;
    }

    public Date getEnSonEklenme() {
        return enSonEklenme;
    }

    public void setEnSonEklenme(Date enSonEklenme) {
        this.enSonEklenme = enSonEklenme;
    }

    public int getToplamKayıtlıKullanıcı() {
        return toplamKayıtlıKullanıcı;
    }

    public void setToplamKayıtlıKullanıcı(int toplamKayıtlıKullanıcı) {
        this.toplamKayıtlıKullanıcı = toplamKayıtlıKullanıcı;
    }

    public int getKullaniciMaxMulkEkleme() {
        return kullaniciMaxMulkEkleme;
    }

    public void setKullaniciMaxMulkEkleme(int kullaniciMaxMulkEkleme) {
        this.kullaniciMaxMulkEkleme = kullaniciMaxMulkEkleme;
    }

    public String getMaxMulkEkleyenKullanici() {
        return maxMulkEkleyenKullanici;
    }

    public void setMaxMulkEkleyenKullanici(String maxMulkEkleyenKullanici) {
        this.maxMulkEkleyenKullanici = maxMulkEkleyenKullanici;
    }
        
    
        
        
        
    
    
    
    
}
