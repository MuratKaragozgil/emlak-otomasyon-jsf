package Veritabani;

import com.mysql.jdbc.Blob;
import java.sql.Date;

public class mulk {
    private String adres,aciklama,satilikmi,onaylandimi,aktifmi;
    private int resimId,ilanid,kategoriid,kimEklemis;
    private Date kayittarihi,  kayitTarihi;
    private int mailID,metrekaresi,fiyatyasi,arsaID,fiyat,yasi,ID;
    private String   link,mailBaslik,mailIcerik,adSoyad,adminMi,sifresi,telNo,   email,kullaniciAdi,konutArsasi,ticariArsasi,sanayiArsasi,tarla,imardurumu, bolge,krediyeUygunluk;
    private java.sql.Blob resim; 
        
        
      
        
               
    
    
    public mulk(int a,String b, Date c, int d, String e, String f,String g,String h){
    
        ilanid=a;
        adres=b;
        kayittarihi=c;
        kategoriid=d;
        aciklama=e;
        satilikmi=f;
        onaylandimi=g;
        aktifmi=h;
       
    }
    
    
    /*Resim Tablosu için*/
    public mulk(int a,String b,int c,java.sql.Blob d)
    {
        resimId=a;
        link=b;
        ID=c;
        resim=d;
    
    
    }    
    
    /*Mülk tablosu için*/
    public mulk(int a,String b, Date c, int d, String e, String f,String g,String h,int i){
    
        ilanid=a;
        adres=b;
        kayittarihi=c;
        kategoriid=d;
        aciklama=e;
        satilikmi=f;
        onaylandimi=g;
        aktifmi=h;
        kimEklemis=i;
       
    }
    /*Villa,daire,dükkan ve fabrika detay tabloları için*/
    public mulk(int a,int b,int c,int d,String e,String f,int g){
    
        ilanid=a;
        metrekaresi=b;
        fiyat=c;
        yasi=d;
        bolge=e;
        krediyeUygunluk=f;
        ID=g;   
        
        
       
    }
    /*Kullanıcı hesabı tablosu*/
    public mulk(int a,String b,String c,String d,String e,String f,String h,Date g)
    {
        ID=a;
        kullaniciAdi =b;       
        email=c;
        telNo=d;
        sifresi=e;
        adres=h;
        adminMi=f;
        kayitTarihi=g;
        
        
    }
    
    /*Başvuru formu tablosu*/
    public mulk(int a,String b,String c,String d,Date e)
    {
        ID=a;
        adSoyad=b;
        email=c;
        telNo=d;
        kayitTarihi=e;
        
    }
    
    /*Mail log tablosu için*/
    public mulk(int a,int b,String c,String d,Date e)
    {
        mailID=a;
        ID=b;
        mailIcerik=c;
        mailBaslik=d;
        kayitTarihi=e;      
        
    }
    /*Arsa detay tablosu için*/
    public mulk(int a,int b,int c,String d,String e,String f,String g,String h,String i,String j,int k,int l)
            {
                ilanid=a;
                metrekaresi=b;
                fiyat=c;
                konutArsasi=d;
                ticariArsasi=e;
                sanayiArsasi=f;
                tarla=g;
                imardurumu=h;
                bolge=i;
                krediyeUygunluk=j;
                yasi=k;
                arsaID=l;
                
                
                
                
                
                
            }
    public int getKimEklemis() {
        return kimEklemis;
    }

    public void setKimEklemis(int kimEklemis) {
        this.kimEklemis = kimEklemis;
    }
    
   
    
    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getAciklama() {
        return aciklama;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public String getSatilikmi() {
        return satilikmi;
    }

    public void setSatilikmi(String satilikmi) {
        this.satilikmi = satilikmi;
    }

    public String getOnaylandimi() {
        return onaylandimi;
    }

    public void setOnaylandimi(String onaylandimi) {
        this.onaylandimi = onaylandimi;
    }

    public String getAktifmi() {
        return aktifmi;
    }

    public void setAktifmi(String aktifmi) {
        this.aktifmi = aktifmi;
    }

    public int getIlanid() {
        return ilanid;
    }

    public void setIlanid(int ilanid) {
        this.ilanid = ilanid;
    }

    public int getKategoriid() {
        return kategoriid;
    }

    public void setKategoriid(int kategoriid) {
        this.kategoriid = kategoriid;
    }

    public String getKayittarihi() {
        return String.valueOf(kayittarihi);
    }

    public void setKayittarihi(Date kayittarihi) {
        this.kayittarihi = kayittarihi;
    }

    public int getMetrekaresi() {
        return metrekaresi;
    }

    public void setMetrekaresi(int metrekaresi) {
        this.metrekaresi = metrekaresi;
    }

    public int getFiyatyasi() {
        return fiyatyasi;
    }

    public void setFiyatyasi(int fiyatyasi) {
        this.fiyatyasi = fiyatyasi;
    }

    public int getArsaID() {
        return arsaID;
    }

    public void setArsaID(int arsaID) {
        this.arsaID = arsaID;
    }

    public String getKonutArsasi() {
        return konutArsasi;
    }

    public void setKonutArsasi(String konutArsasi) {
        this.konutArsasi = konutArsasi;
    }

    public String getTicariArsasi() {
        return ticariArsasi;
    }

    public void setTicariArsasi(String ticariArsasi) {
        this.ticariArsasi = ticariArsasi;
    }

    public String getSanayiArsasi() {
        return sanayiArsasi;
    }

    public void setSanayiArsasi(String sanayiArsasi) {
        this.sanayiArsasi = sanayiArsasi;
    }

    public String getTarla() {
        return tarla;
    }

    public void setTarla(String tarla) {
        this.tarla = tarla;
    }

    public String getImardurumu() {
        return imardurumu;
    }

    public void setImardurumu(String imardurumu) {
        this.imardurumu = imardurumu;
    }

    public String getBolge() {
        return bolge;
    }

    public void setBolge(String bolge) {
        this.bolge = bolge;
    }

    public String getKrediyeUygunluk() {
        return krediyeUygunluk;
    }

    public void setKrediyeUygunluk(String krediyeUygunluk) {
        this.krediyeUygunluk = krediyeUygunluk;
    }

    public int getFiyat() {
        return fiyat;
    }

    public void setFiyat(int fiyat) {
        this.fiyat = fiyat;
    }

    public int getYasi() {
        return yasi;
    }

    public void setYasi(int yasi) {
        this.yasi = yasi;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Date getKayitTarihi() {
        return kayitTarihi;
    }

    public void setKayitTarihi(Date kayitTarihi) {
        this.kayitTarihi = kayitTarihi;
    }

    public String getAdminMi() {
        return adminMi;
    }

    public void setAdminMi(String adminMi) {
        this.adminMi = adminMi;
    }

    public String getSifresi() {
        return sifresi;
    }

    public void setSifresi(String sifresi) {
        this.sifresi = sifresi;
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKullaniciAdi() {
        return kullaniciAdi;
    }

    public void setKullaniciAdi(String kullaniciAdi) {
        this.kullaniciAdi = kullaniciAdi;
    }

    public String getAdSoyad() {
        return adSoyad;
    }

    public void setAdSoyad(String adSoyad) {
        this.adSoyad = adSoyad;
    }

    public int getMailID() {
        return mailID;
    }

    public void setMailID(int mailID) {
        this.mailID = mailID;
    }

    public String getMailBaslik() {
        return mailBaslik;
    }

    public void setMailBaslik(String mailBaslik) {
        this.mailBaslik = mailBaslik;
    }

    public String getMailIcerik() {
        return mailIcerik;
    }

    public void setMailIcerik(String mailIcerik) {
        this.mailIcerik = mailIcerik;
    }

    public int getResimId() {
        return resimId;
    }

    public void setResimId(int resimId) {
        this.resimId = resimId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public java.sql.Blob getResim() {
        return resim;
    }

    public void setResim(java.sql.Blob resim) {
        this.resim = resim;
    }

   
    
    
}
