package Veritabani;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.lang.Exception;
import java.sql.Blob;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;


public class SqlKomutları extends Database {
    
        private static Connection con=null;
        private static PreparedStatement ps=null;
        String esifre,ysifre;
        private String email,adsoyad,telno,adminmi="h",ID,kayitEmail,solAdsoyad,solTelno,solEmail,neredeyimString;
        private ResultSet rs=null;
        private ResultSet rsMail=null;
        private String binametrekare="",binafiyat="",binabölge="",binakredi="",binayasi="",binaadres="",binaaciklama="",binasatilikmi="",binakategoriID="";
        private String metrekare="",fiyat="",konutarsasi="",ticariarac="",sanayiarsasi="",tarla="",imardurumu="",bölge="",kredi="",yasi="",adres="",aciklama="",tarih="",satilikmi="",kategoriID="";
        private String mail,sifre,mailIcerik,mailBaslik;
        private List <mulk> arsalistesi;
        private List <mulk> dairelistesi;
        private List <mulk> villalistesi;
        private List <mulk> dukkanlistesi;
        private List <mulk> fabrikalistesi;
        private List <mulk> mulklistesi;
        private List <mulk> mulklistesi2;
        private List <mulk> mulklistesiAra;
        private List <form> formlistesi;
        private UploadedFile yüklenecekDosya;
        private UploadedFile yüklenecekDosya2;
        private StreamedContent dbImage;    
        
        
        public boolean login(String userName , String passWord)
        {
            try{
            con=getConnection();
            ps=con.prepareStatement("select * from KullaniciHesabi where KullaniciAdi=? and Sifresi=?");
            ps.setString(1, userName);
            ps.setString(2, passWord);
            
            
            
            rs = ps.executeQuery();
            
            if(rs.next())// found
            {
                setkAdi(rs.getString("KullaniciAdi"));
                setkSifre(rs.getString("Sifresi"));
                setkId(String.valueOf(rs.getInt("ID")));
                adminmi=rs.getString("AdminMi");
                 return true;
            }

            else
            {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mesaj", "Kullanıcı Adı ve ya Şifre Hatalı!");
                RequestContext.getCurrentInstance().showMessageInDialog(message);
                return false;
            }
              
            
        }catch(Exception ex){
                setMesaj(ex.getMessage());
            return false;
        }finally {
            Database.close(con);
        }

    }
        
        public void resimUp() throws Exception
    {
         InputStream fin2 = yüklenecekDosya.getInputstream();
                con = getConnection();
                PreparedStatement SQLKomut = con.prepareStatement("insert into resimler (link,ilanid,resim) values(?,?,?)");
                SQLKomut.setString(1, yüklenecekDosya.getFileName());
                SQLKomut.setBinaryStream(3, fin2, yüklenecekDosya.getSize());
                SQLKomut.setInt(2, 3);
                SQLKomut.executeUpdate();

        close(con);
    }
    
    public void resimGoster() throws Exception
    {
        con= getConnection();
        Blob blob; 
        
        PreparedStatement SQLKomut = con.prepareStatement("select * from resimler where resimid=8");
        rs =  SQLKomut.executeQuery();
        rs.next();
       blob= rs.getBlob(4);
        
       InputStream dbStream = blob.getBinaryStream(); //Get inputstream of a blob eg javax.sql.Blob.getInputStream() API
        dbImage = new DefaultStreamedContent(dbStream, "image/jpeg");
        
        dbStream.reset();
        dbStream.close();
        blob.free();
        rs.close();
    }
        
        public void arsaEkle() throws Exception
        {
           String userID = getkId(); // sessiondan alacaz
           con = getConnection();
       
PreparedStatement SQLKomut = con.prepareStatement("insert into mulk (adres,kayittarihi,kategoriID,aciklama,satilikmi,onaylandimi,"
        + "aktifmi,kimeklemis) values(?,?,?,?,?,?,?,?)");
            SQLKomut.setString(1,adres);
            SQLKomut.setDate(2,Date.valueOf(LocalDate.now()));
            SQLKomut.setInt(3,0);
            SQLKomut.setString(4,aciklama);
            SQLKomut.setString(5,"E");
            SQLKomut.setString(6,"H");
            SQLKomut.setString(7,"E");
            SQLKomut.setInt(8,Integer.parseInt(userID));
            
            SQLKomut.executeUpdate();
            
 PreparedStatement SQLKomut2 = con.prepareStatement("Select  * from mulk order by ilanid desc LIMIT 1;");           
            
             rs = SQLKomut2.executeQuery();
            rs.next();
       String mulkId=String.valueOf(rs.getInt(1));
              
       InputStream fin2 = yüklenecekDosya.getInputstream();
             
                PreparedStatement SQLKomut4 = con.prepareStatement("insert into resimler (link,ilanid,resim) values(?,?,?)");
                SQLKomut4.setString(1, yüklenecekDosya.getFileName());
                SQLKomut4.setBinaryStream(3, fin2, yüklenecekDosya.getSize());
                SQLKomut4.setInt(2, Integer.parseInt(mulkId));
                SQLKomut4.executeUpdate();
            
PreparedStatement SQLKomut3 = con.prepareStatement("insert into arsadetaylar "
        + "(metrekaresi,fiyat,konutArsasi,ticariArac,sanayiarsasi,tarla,imarDurumu,bolge,krediyeuygunluk,yasi,arsaID) values(?,?,?,?,?,?,?,?,?,?,?)");       
            SQLKomut3.setInt(1, Integer.parseInt(metrekare));
            SQLKomut3.setInt(2, Integer.parseInt(fiyat));
            SQLKomut3.setString(3, konutarsasi);
            SQLKomut3.setString(4, ticariarac);
            SQLKomut3.setString(5, sanayiarsasi);
            SQLKomut3.setString(6, tarla);
            SQLKomut3.setString(7, imardurumu);
            SQLKomut3.setString(8, bölge);
            SQLKomut3.setString(9, kredi);
            SQLKomut3.setInt(10, Integer.parseInt(yasi));
            SQLKomut3.setInt(11, Integer.parseInt(mulkId));
            
            SQLKomut3.executeUpdate();
           
            
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mesaj", "Mülk Başarıyla Eklendi!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
            
            close(con);
        
        }
        
        public void binaEkle () throws Exception
        {
            String userID = getkId(); // sessiondan alacaz
           con = getConnection();

PreparedStatement SQLKomut = con.prepareStatement("insert into mulk (adres,kayittarihi,kategoriID,aciklama,satilikmi,onaylandimi,"
        + "aktifmi,kimeklemis) values(?,?,?,?,?,?,?,?)");
            SQLKomut.setString(1,binaadres);
            SQLKomut.setDate(2,Date.valueOf(LocalDate.now()));
            SQLKomut.setInt(3,Integer.parseInt(binakategoriID));
            SQLKomut.setString(4,binaaciklama);
            SQLKomut.setString(5,binasatilikmi);
            SQLKomut.setString(6,"H");
            SQLKomut.setString(7,"E");
            SQLKomut.setInt(8,Integer.parseInt(userID));
            
            SQLKomut.executeUpdate();
            
 PreparedStatement SQLKomut2 = con.prepareStatement("Select  * from mulk order by ilanid desc LIMIT 1;");
             rs = SQLKomut2.executeQuery();// mülke son eklenen (yukarıda eklediğimizin) id sini al ve villaysa oraya bağlayacak şekilde ekle 
            
            rs.next();
             String mulkId=String.valueOf(rs.getInt(1));
             
              InputStream fin2 = yüklenecekDosya2.getInputstream();
              PreparedStatement SQLKomut4 = con.prepareStatement("insert into resimler (link,ilanid,resim) values(?,?,?)");
                SQLKomut4.setString(1, yüklenecekDosya2.getFileName());
                SQLKomut4.setBinaryStream(3, fin2, yüklenecekDosya2.getSize());
                SQLKomut4.setInt(2, Integer.parseInt(mulkId));
                SQLKomut4.executeUpdate();
                
             PreparedStatement SQLKomut3=null;
            switch(binakategoriID)
            {
                case "1":
                   SQLKomut3  = con.prepareStatement("insert into dairedetaylar "
                           + "(metrekaresi,fiyat,yasi,bolge,krediyeuygunluk,daireId) values(?,?,?,?,?,?)");       
SQLKomut3.setInt(1, Integer.parseInt(binametrekare));
            SQLKomut3.setInt(2, Integer.parseInt(binafiyat));
            SQLKomut3.setInt(3, Integer.parseInt(binayasi));
            SQLKomut3.setString(4, binabölge);
            SQLKomut3.setString(5, binakredi);
            SQLKomut3.setInt(6, Integer.parseInt(mulkId));
             SQLKomut3.executeUpdate();
                    break;
                
                case "2":
                    SQLKomut3  = con.prepareStatement("insert into dukkandetaylar "
                            + "(metrekaresi,fiyat,yasi,bolge,krediyeuygunluk,dukkanId) values(?,?,?,?,?,?)");       
SQLKomut3.setInt(1, Integer.parseInt(binametrekare));
            SQLKomut3.setInt(2, Integer.parseInt(binafiyat));
            SQLKomut3.setInt(3, Integer.parseInt(binayasi));
            SQLKomut3.setString(4, binabölge);
            SQLKomut3.setString(5, binakredi);
            SQLKomut3.setInt(6, Integer.parseInt(mulkId));
             SQLKomut3.executeUpdate();
                    break;
                    
                case "3":
                    SQLKomut3  = con.prepareStatement("insert into fabrikadetaylar "
                            + "(metrekaresi,fiyat,yasi,bolge,krediyeuygunluk,fabrikaid) values(?,?,?,?,?,?)");       
SQLKomut3.setInt(1, Integer.parseInt(binametrekare));
            SQLKomut3.setInt(2, Integer.parseInt(binafiyat));
            SQLKomut3.setInt(3, Integer.parseInt(binayasi));
            SQLKomut3.setString(4, binabölge);
            SQLKomut3.setString(5, binakredi);
            SQLKomut3.setInt(6, Integer.parseInt(mulkId));
             SQLKomut3.executeUpdate();
                    break;
                    
                case "4":
                    SQLKomut3  = con.prepareStatement("insert into villadetaylar (metrekaresi,fiyat,yasi,bolge,krediyeuygunluk,villaid) values(?,?,?,?,?,?)");       
SQLKomut3.setInt(1, Integer.parseInt(binametrekare));
            SQLKomut3.setInt(2, Integer.parseInt(binafiyat));
            SQLKomut3.setInt(3, Integer.parseInt(binayasi));
            SQLKomut3.setString(4, binabölge);
            SQLKomut3.setString(5, binakredi);
            SQLKomut3.setInt(6, Integer.parseInt(mulkId));
            SQLKomut3.executeUpdate();
                    break;
            }

              FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mesaj", "Mülk Başarıyla Eklendi!");
                RequestContext.getCurrentInstance().showMessageInDialog(message);

            close(con);
        
        }
        
        public String hesapSil()
        {
              
            String kAdi = getkAdi();
            String hSifre = getkSifre();
            
                con = getConnection();
            if(hSifre.equals(sifre)) // sifre kullanıcıdan alınacak
            try
            {
               PreparedStatement SQLKomut = con.prepareStatement("delete from kullanicihesabi where KullaniciAdi=? and sifresi=?");
                SQLKomut.setString(1, kAdi);
                SQLKomut.setString(2, hSifre);
                SQLKomut.executeUpdate();
                  FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mesaj", "Silme İşlemi Başarılı!!" + sifre);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
        
                setkAdi(null);
                setkId(null);
                setkSifre(null);
        
               return "index";
            }
            catch(Exception ex)
            {
                
            }
            finally
            {
                close(con);
                
            }
            return "";
        }
        
        public void mailDegistir() throws Exception
        {
           
            String kAdi = getkAdi();
            String kSifre = getkSifre();

            con = getConnection();


               PreparedStatement SQLKomut = con.prepareStatement("Update kullaniciHesabi set email=? where kullaniciAdi=? and sifresi=?");
                SQLKomut.setString(1,mail);
                SQLKomut.setString(2,kAdi);
                SQLKomut.setString(3, kSifre);
                
                SQLKomut.executeUpdate();
            

                close(con);

            
        }
        
        public void sifreDegistir()
        {
           
            String kAdi = getkAdi();
            String kSifre = getkSifre();
            
            if(!esifre.equals(kSifre))
            {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mesaj", "Hatalı Şifre!");
        RequestContext.getCurrentInstance().showMessageInDialog(message);
                return ;
            }
           
             con=getConnection();
                 
            try
            {
        PreparedStatement SQLKomut = con.prepareStatement("Update kullaniciHesabi set sifresi=? where Sifresi=? and KullaniciAdi=?");
        SQLKomut.setString(1, ysifre);
        SQLKomut.setString(2, esifre);
        SQLKomut.setString(3, kAdi);
        SQLKomut.executeUpdate();
        
                setkSifre(ysifre);
        
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mesaj", "Şifre Değişmi Başarılı");
        RequestContext.getCurrentInstance().showMessageInDialog(message); // pop up mesaj
        
        
        }
            
            catch(Exception e)
            {
                 FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mesaj", "Şifre Değişmi Başarısız");
         
        RequestContext.getCurrentInstance().showMessageInDialog(message);
            }
            
            finally
            {
             close(con);   
            }
        }
      
      public boolean veriKaydet()
    {    
           con=getConnection();
                 
    try
    {
        PreparedStatement SQLKomut = con.prepareStatement("Select * from kullaniciHesabi where kullaniciAdi=?");
        SQLKomut.setString(1,this.getUserName());
        
        ResultSet rs=null;
         rs = SQLKomut.executeQuery();
         if (rs.next()) // found
            {
              this.setMesaj("<script> alert('Bu Kullanıcı İsmi Daha Önce Alınmış!') </script>");
              return false;
            }
           
      
    SQLKomut = con.prepareStatement("");
    SQLKomut = con.prepareStatement("INSERT INTO KullaniciHesabi(KullaniciAdi,Email,TelNo,Sifresi,"
            + "Adres,AdminMi,KayitTarihi) VALUES (?,?,?,?,?,?,?)");

        SQLKomut.setString(1, this.getUserName());
        SQLKomut.setString(2, this.getKayitEmail());
        SQLKomut.setString(3, this.getPhoneNo());
        SQLKomut.setString(4, this.getPassWord());
        SQLKomut.setString(5, this.getAdress());
        SQLKomut.setString(6, "H");
        SQLKomut.setDate(7,Date.valueOf(LocalDate.now()));
        
             SQLKomut.executeUpdate();
             con.commit();
             con.close(); 
             return true;
    }
    catch(Exception ex)
    {
        
    }
    
    finally
    {
        close(con);
    }
    return false;
    }
      
      public void formGonder()
      {
          con=getConnection();
                 
    try
    {
   PreparedStatement SQLKomut = con.prepareStatement("INSERT INTO basvuruFormu(adSoyad,mailAdresi,telno,basvuruTarihi)"
           + " VALUES (?,?,?,?)");

        SQLKomut.setString(1, solAdsoyad);
        SQLKomut.setString(2, solEmail);
        SQLKomut.setString(3, solTelno);
        SQLKomut.setDate(4, Date.valueOf(LocalDate.now()));
             SQLKomut.executeUpdate();
             con.commit();
             con.close(); 
             
    }
    catch(Exception ex)
    {
        
    }
    
    finally
    {
        close(con);
    }
      }
      
    public void mulkDetayAl() throws Exception
            {
         con=getConnection();
         PreparedStatement SQLKomut=null;
         mulklistesi = new ArrayList<>();
         String id=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("mulkID");
         String kategori=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("kategoriID");
         Blob blob;
              switch(kategori)
              {
                case "0":
                      SQLKomut = con.prepareStatement("select arsadetaylar.metrekaresi, arsadetaylar.fiyat, arsadetaylar.konutArsasi,  \n" +
"arsadetaylar.ticariArac, arsadetaylar.sanayiArsasi,arsadetaylar.tarla,arsadetaylar.imarDurumu, \n" +
"arsadetaylar.bolge,arsadetaylar.krediyeuygunluk, arsadetaylar.yasi,mulk.adres,mulk.aciklama,mulk.kayittarihi, \n" +
"kullanicihesabi.TelNo,kullanicihesabi.Email,resimler.resim\n" +
"FROM mulk  \n" +
"INNER JOIN arsadetaylar  \n" +
"ON arsadetaylar.arsaID=mulk.ilanID \n" +
"INNER JOIN kullanicihesabi \n" +
"ON mulk.kimEklemis=kullanicihesabi.ID \n" +
"LEFT JOIN resimler\n" +
"on resimler.ilanid=mulk.ilanid\n" +
" WHERE mulk.ilanid = ?");
                SQLKomut.setInt(1, Integer.parseInt(id));
                rs=SQLKomut.executeQuery();   
                    
                    while(rs.next()){
                 metrekare=String.valueOf(rs.getInt(1));
                 fiyat=String.valueOf(rs.getInt(2)); 
                 konutarsasi=rs.getString(3);
                 ticariarac=rs.getString(4);
                 sanayiarsasi=rs.getString(5);
                tarla=rs.getString(6);
                imardurumu=rs.getString(7);
                 bölge=rs.getString(8);
                 kredi=rs.getString(9);
                 yasi=String.valueOf(rs.getInt(10));
                adres=rs.getString(11);
                aciklama=rs.getString(12);
                 tarih=String.valueOf(rs.getDate(13));
                email=rs.getString(15);
                telno=rs.getString(14);
                blob = rs.getBlob(16);
                
                if(blob == null)
                {
                    PreparedStatement SQLKomut2 = con.prepareStatement("select resim from resimler where resimid=-1");
                    ResultSet rs2 = null;
                    rs2 = SQLKomut2.executeQuery();
                    rs2.next();
                    blob = rs2.getBlob(1);
                }
                
                InputStream dbStream = blob.getBinaryStream(); //Get inputstream of a blob eg javax.sql.Blob.getInputStream() API
                dbImage = new DefaultStreamedContent(dbStream, "image/jpeg");
               
        
                dbStream.reset();
                dbStream.close();
                blob.free();
               
                
                if(kredi.equals("E"))
                    kredi="Uygun";
                
                else
                    kredi="Uygun Değil";
                
                if(konutarsasi.equals("E"))
                    konutarsasi="Evet";
                else
                    konutarsasi="Hayır";
                
                if(ticariarac.equals("E"))
                    ticariarac="Evet";
                
                else
                    ticariarac="Hayır";
                
                if(sanayiarsasi.equals("E"))
                    sanayiarsasi="Evet";
                
                else
                    sanayiarsasi="Hayır";
                
                 if(tarla.equals("E"))
                    tarla="Evet";
                
                else
                    tarla="Hayır";
                
                 if(imardurumu.equals("E"))
                    imardurumu="Evet";
                
                else
                    imardurumu="Hayır";
                return;
                }
                    break;
                      
                case "1":
                      SQLKomut = con.prepareStatement("select dairedetaylar.metrekaresi,dairedetaylar.fiyat,dairedetaylar.yasi,dairedetaylar.bolge,dairedetaylar.krediyeuygunluk,mulk.adres,  \n" +
"mulk.aciklama,mulk.kayittarihi,kullanicihesabi.TelNo,kullanicihesabi.Email,resimler.resim\n" +
"FROM mulk  \n" +
"INNER JOIN dairedetaylar  \n" +
"ON dairedetaylar.daireID=mulk.ilanID  \n" +
"INNER JOIN kullanicihesabi \n" +
"ON mulk.kimEklemis=kullanicihesabi.ID \n" +
"LEFT JOIN resimler\n" +
"on resimler.ilanid=mulk.ilanid\n" +
" WHERE mulk.ilanid = ?");
                    break;
                
                case "2":
                      SQLKomut = con.prepareStatement("select dukkandetaylar.metrekaresi,dukkandetaylar.fiyat,dukkandetaylar.yasi,dukkandetaylar.bolge,dukkandetaylar.krediyeuygunluk,mulk.adres,  \n" +
"mulk.aciklama,mulk.kayittarihi,kullanicihesabi.TelNo,kullanicihesabi.Email,resimler.resim\n" +
"FROM mulk  \n" +
"INNER JOIN dukkandetaylar  \n" +
"ON dukkandetaylar.dukkanid=mulk.ilanID  \n" +
"INNER JOIN kullanicihesabi \n" +
"ON mulk.kimEklemis=kullanicihesabi.ID \n" +
"LEFT JOIN resimler\n" +
"on resimler.ilanid=mulk.ilanid\n" +
" WHERE mulk.ilanid = ?");
                      break;
                  
                case "3":
                      SQLKomut = con.prepareStatement("select fabrikadetaylar.metrekaresi,fabrikadetaylar.fiyat,fabrikadetaylar.yasi,fabrikadetaylar.bolge,fabrikadetaylar.krediyeuygunluk,mulk.adres,  \n" +
"mulk.aciklama,mulk.kayittarihi,kullanicihesabi.TelNo,kullanicihesabi.Email,resimler.resim\n" +
"FROM mulk  \n" +
"INNER JOIN fabrikadetaylar  \n" +
"ON fabrikadetaylar.fabrikaid=mulk.ilanID  \n" +
"INNER JOIN kullanicihesabi \n" +
"ON mulk.kimEklemis=kullanicihesabi.ID \n" +
"LEFT JOIN resimler\n" +
"on resimler.ilanid=mulk.ilanid\n" +
" WHERE mulk.ilanid = ?");
                      break;
                    
                case "4":
                      SQLKomut = con.prepareStatement("select villadetaylar.metrekaresi,villadetaylar.fiyat,villadetaylar.yasi,villadetaylar.bolge,villadetaylar.krediyeuygunluk,mulk.adres,  \n" +
"mulk.aciklama,mulk.kayittarihi,kullanicihesabi.TelNo,kullanicihesabi.Email,resimler.resim\n" +
"FROM mulk  \n" +
"INNER JOIN villadetaylar  \n" +
"ON villadetaylar.villaid=mulk.ilanID  \n" +
"INNER JOIN kullanicihesabi \n" +
"ON mulk.kimEklemis=kullanicihesabi.ID \n" +
"LEFT JOIN resimler\n" +
"on resimler.ilanid=mulk.ilanid\n" +
" WHERE mulk.ilanid = ?");
                      break;
                    
                default:
                    SQLKomut = con.prepareStatement("");
              }
              
              SQLKomut.setInt(1, Integer.parseInt(id));
             
             rs=SQLKomut.executeQuery();    
             
             
             
             while(rs.next()){
                metrekare=String.valueOf(rs.getInt(1));
                fiyat=String.valueOf(rs.getInt(2));
                yasi=String.valueOf(rs.getInt(3));
                bölge=rs.getString(4);
                kredi=rs.getString(5);
                adres=rs.getString(6);
                aciklama=rs.getString(7);
                tarih=String.valueOf(rs.getDate(8));
                telno=rs.getString(9);
                email=rs.getString(10);
                blob = rs.getBlob(11);
                
                if(blob == null)
                {
                    PreparedStatement SQLKomut2 = con.prepareStatement("select resim from resimler where resimid=-1");
                    ResultSet rs2 = null;
                    rs2 = SQLKomut2.executeQuery();
                    rs2.next();
                    blob = rs2.getBlob(1);
                }
                InputStream dbStream = blob.getBinaryStream(); //Get inputstream of a blob eg javax.sql.Blob.getInputStream() API
                dbImage = new DefaultStreamedContent(dbStream, "image/jpeg");
        
                dbStream.reset();
                dbStream.close();
                blob.free();
                
             }
              if(kredi.equals("E"))
                    kredi="Uygun";
                
                else
                    kredi="Uygun Değil";

            close(con);
    }
    
    public void mulkGoruntule()
    {
        mulklistesi = new ArrayList<>();
        arsalistesi = new ArrayList<>();
        dairelistesi = new ArrayList<>();
        villalistesi = new ArrayList<>();
        dukkanlistesi = new ArrayList<>();
        fabrikalistesi = new ArrayList<>();
        
        con = getConnection();
          try{
            PreparedStatement SQLKomut = con.prepareStatement("select * from mulk");
            
             rs=SQLKomut.executeQuery();    
             while(rs.next()){
                 mulklistesi.add(new mulk(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));
                 switch(rs.getInt(4))
                 {
                    case 0:
                arsalistesi.add(new mulk(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));
                         break;
                    case 1 :
                dairelistesi.add(new mulk(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));
                         break;
                    case 2:
                dukkanlistesi.add(new mulk(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));         
                         break;
                         
                    case 3:
                fabrikalistesi.add(new mulk(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));
                         break;
                          
                    case 4:
                villalistesi.add(new mulk(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));         
                    break;
                
                 }
                    
             }
          }catch(Exception e){  
         }
          
          finally
          {
          
              close(con);
          }
            
    }
    
    public void mulkGoruntule2() throws Exception
    {
        mulklistesi2 = new ArrayList<>();
        ResultSet rs2=null;

        Connection con2 = getConnection();
        
            PreparedStatement SQLKomut2 = con2.prepareStatement("select * from mulk where onaylandimi='H'");
            
             rs2=SQLKomut2.executeQuery();   
             
             while(rs2.next()){
                 mulklistesi2.add(new mulk(rs2.getInt(1), rs2.getString(2), rs2.getDate(3), rs2.getInt(4), rs2.getString(5), rs2.getString(6), rs2.getString(7), rs2.getString(8)));                              
            }
          
            
    
             
               close(con2);
}
    
    public void aramaYap() throws Exception
    {
        con=getConnection();
         mulklistesiAra = new ArrayList<>();
   String aramaMetni=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("ara");
            PreparedStatement SQLKomut = con.prepareStatement("Select * from mulk WHERE adres LIKE '%"+aramaMetni+"%' or aciklama LIKE '%"+aramaMetni+"%' order by ilanid");
            
             rs=SQLKomut.executeQuery();
             
             while(rs.next()){
                 mulklistesiAra.add(new mulk(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));
             }

        close(con);
        
    }
    
    public String tablolaraGit()
    {
        setkAdi(null);
        setkId(null);
        setkSifre(null);
        
        return "tablolar.xhtml?faces-redirect=true";
    }
    
    public void kategoriGoruntule() throws Exception
    {
        String kid,sat;
        kid = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("KategoriID");
        sat = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("satilik");
        
        switch(kid)
        {
            case "0":
                if(sat.equalsIgnoreCase("e"))
                    neredeyimString="Satılık Arsalar";
                
                else
                    neredeyimString="Kiralık Arsalar";
                break;
                
            case "1":
                if(sat.equalsIgnoreCase("e"))
                    neredeyimString="Satılık Daireler";
                
                else
                    neredeyimString="Kiralık Daireler";
                break;
                
            case "2":
                if(sat.equalsIgnoreCase("e"))
                    neredeyimString="Satılık Dükkanlar";
                
                else
                    neredeyimString="Kiralık Dükkanlar";
                break;
                
            case "3":
                if(sat.equalsIgnoreCase("e"))
                    neredeyimString="Satılık Fabrikalar";
                
                else
                    neredeyimString="Kiralık Fabrikalar";
                break;
                
            case "4":
                if(sat.equalsIgnoreCase("e"))
                    neredeyimString="Satılık Villalar";
                
                else
                    neredeyimString="Kiralık Villalar";
                break;
                
            default:
                neredeyimString="Hatalı Kategori Seçimi!";                
        }
        
        con=getConnection();
         mulklistesi = new ArrayList<>();
   
            PreparedStatement SQLKomut = con.prepareStatement("Select * From mulk where kategoriID=? and satilikMi=? and onaylandimi='E'");
            
            SQLKomut.setInt(1, Integer.parseInt(kid));
            SQLKomut.setString(2, sat);
             rs=SQLKomut.executeQuery();    
             while(rs.next()){
                 mulklistesi.add(new mulk(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));
             }

        close(con);
    
    }

    public void mulkOnayla() throws Exception
    {
        con = getConnection();

PreparedStatement SQLKomut = con.prepareStatement("Update mulk set onaylandimi='E' where onaylandimi='H'");

        SQLKomut.executeUpdate();
        
        close(con);
    }
    
    public boolean hesapKontrol()
    {
        try
    {
        if(getkAdi().isEmpty()|| getkId().isEmpty() || getkSifre().isEmpty())
            return true;
    }
    catch(NullPointerException ex)
    {
        return true;
    }
        return false;
    }
    
    public void mailGonder() throws Exception
    {
        
        con=getConnection();

            PreparedStatement SQLKomut = con.prepareStatement("Select email From kullaniciHesabi");
             rsMail=SQLKomut.executeQuery();    

        int size = 0;
    rsMail.last();
    size = rsMail.getRow();
    rsMail.beforeFirst();
    
    InternetAddress[] adresler = new InternetAddress[size];

        final String username = "ofkproje@gmail.com";
		final String password = "ofkproje123";
 
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
 
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
                        @Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
                int i=0;
                    
                
                    while(rsMail.next())
                    {
                          adresler[i] = new InternetAddress(rsMail.getString(1));
                          i++;
                    }

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ofkproje@gmail.com"));
                        //message.setRecipients(Message.RecipientType.TO, adresler);
                       message.setRecipient(Message.RecipientType.TO, new InternetAddress("omerfarukkirli1994@gmail.com"));
                        message.addRecipients(Message.RecipientType.TO, adresler);
			message.setSubject(mailBaslik);
			message.setText(mailIcerik);
 
			Transport.send(message);
                        
        PreparedStatement SQLKomut2 = con.prepareStatement("insert into maillog (kullaniciID,mailicerik,mailbaslik,"
                + "gonderilmeTarihi) values(?,?,?,?)");
      
        SQLKomut2.setInt(1, Integer.parseInt(getkId()));
        SQLKomut2.setString(2, mailIcerik);
        SQLKomut2.setString(3, mailBaslik);
        SQLKomut2.setDate(4,Date.valueOf(LocalDate.now()));
        
        SQLKomut2.executeUpdate();
        
        close(con);
   }

    public void mailGonderKullanici() throws Exception
    {
                
        con=getConnection();

            PreparedStatement SQLKomut = con.prepareStatement("Select email From kullaniciHesabi where adminmi='E'");
             rsMail=SQLKomut.executeQuery();    

        int size = 0;
    rsMail.last();
    size = rsMail.getRow();
    rsMail.beforeFirst();
    
    InternetAddress[] adresler = new InternetAddress[size];

        final String username = "ofkproje@gmail.com";
		final String password = "ofkproje123";
 
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
 
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
                        @Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
                int i=0;
                    
                
                    while(rsMail.next())
                    {
                          adresler[i] = new InternetAddress(rsMail.getString(1));
                          i++;
                    }

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ofkproje@gmail.com"));
                        message.setRecipient(Message.RecipientType.TO, new InternetAddress("omerfarukkirli1994@gmail.com"));
                        message.addRecipients(Message.RecipientType.TO, adresler);
                        message.addRecipients(Message.RecipientType.TO, adresler);
			message.setSubject(mailBaslik);
			message.setText(mailIcerik);
 
			Transport.send(message);
                        
        PreparedStatement SQLKomut2 = con.prepareStatement("insert into maillog (kullaniciID,mailicerik,mailbaslik,gonderilmeTarihi) values(?,?,?,?)");
      
        SQLKomut2.setInt(1, Integer.parseInt(getkId()));
        SQLKomut2.setString(2, mailIcerik);
        SQLKomut2.setString(3, mailBaslik);
        SQLKomut2.setDate(4,Date.valueOf(LocalDate.now()));
        
        SQLKomut2.executeUpdate();
                        
      close(con);
    }
    
    public String raporlamayaGit()
    {
        setkAdi(null);
        setkSifre(null);
        setkId(null);
        return "raporlama.xhtml?faces-redirect=true";
    }
    
    public void formGetir() throws SQLException
    {
        con=getConnection();
        formlistesi = new ArrayList<>();
        
            PreparedStatement SQLKomut = con.prepareStatement("Select * From basvuruFormu");
             rs=SQLKomut.executeQuery();    
             while(rs.next())
             {
                 formlistesi.add(new form(rs.getString(2), rs.getString(3), rs.getString(4), rs.getDate(5)));
             }
             
             close(con);
    }
    
    public void formSil() throws SQLException
    {
         con=getConnection();
          PreparedStatement SQLKomut = con.prepareStatement("delete from basvuruformu ");
          SQLKomut.executeUpdate();
          close(con);
    }
    
    public void anasayfa() throws SQLException
    {
         con=getConnection();
         mulklistesiAra = new ArrayList<>();
            PreparedStatement SQLKomut = con.prepareStatement("Select * from mulk limit 6");
            
             rs=SQLKomut.executeQuery();
             
             while(rs.next()){
                 mulklistesiAra.add(new mulk(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));
             }

        close(con);
    }
    
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdsoyad() {
        return adsoyad;
    }

    public void setAdsoyad(String adsoyad) {
        this.adsoyad = adsoyad;
    }

    public String getTelno() {
        return telno;
    }

    public void setTelno(String telno) {
        this.telno = telno;
    }

    public ResultSet getRs() {
        return rs;
    }

    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    public String getAdminmi() {
        return adminmi;
    }

    public void setAdminmi(String adminmi) {
        this.adminmi = adminmi;
    }

    public List<mulk> getMulklistesi() {
        return mulklistesi;
    }

    public String getMetrekare() {
        return metrekare;
    }

    public void setMetrekare(String metrekare) {
        this.metrekare = metrekare;
    }

    public String getFiyat() {
        return fiyat;
    }

    public void setFiyat(String fiyat) {
        this.fiyat = fiyat;
    }

    public String getKonutarsasi() {
        return konutarsasi;
    }

    public void setKonutarsasi(String konutarsasi) {
        this.konutarsasi = konutarsasi;
    }

    public String getTicariarac() {
        return ticariarac;
    }

    public void setTicariarac(String ticariarac) {
        this.ticariarac = ticariarac;
    }

    public String getSanayiarsasi() {
        return sanayiarsasi;
    }

    public void setSanayiarsasi(String sanayiarsasi) {
        this.sanayiarsasi = sanayiarsasi;
    }

    public String getTarla() {
        return tarla;
    }

    public void setTarla(String tarla) {
        this.tarla = tarla;
    }

    public String getImardurumu() {
        return imardurumu;
    }

    public void setImardurumu(String imardurumu) {
        this.imardurumu = imardurumu;
    }

    public String getBölge() {
        return bölge;
    }

    public void setBölge(String bölge) {
        this.bölge = bölge;
    }

    public String getKredi() {
        return kredi;
    }

    public void setKredi(String kredi) {
        this.kredi = kredi;
    }

    public String getYasi() {
        return yasi;
    }

    public void setYasi(String yasi) {
        this.yasi = yasi;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getAciklama() {
        return aciklama;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public String getTarih() {
        return tarih;
    }

    public void setTarih(String tarih) {
        this.tarih = tarih;
    }

    public List<mulk> getArsalistesi() {
        return arsalistesi;
    }

    public List<mulk> getDairelistesi() {
        return dairelistesi;
    }

    public List<mulk> getVillalistesi() {
        return villalistesi;
    }

    public List<mulk> getDukkanlistesi() {
        return dukkanlistesi;
    }

    public List<mulk> getFabrikalistesi() {
        return fabrikalistesi;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getEsifre() {
        return esifre;
    }

    public void setEsifre(String esifre) {
        this.esifre = esifre;
    }

    public String getYsifre() {
        return ysifre;
    }

    public void setYsifre(String ysifre) {
        this.ysifre = ysifre;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getSifre() {
        return sifre;
    }

    public void setSifre(String sifre) {
        this.sifre = sifre;
    }

    public String getSatilikmi() {
        return satilikmi;
    }

    public List<form> getFormlistesi() {
        return formlistesi;
    }

    public void setFormlistesi(List<form> formlistesi) {
        this.formlistesi = formlistesi;
    }

    public void setSatilikmi(String satilikmi) {
        this.satilikmi = satilikmi;
    }

    public String getKategoriID() {
        return kategoriID;
    }

    public void setKategoriID(String kategoriID) {
        this.kategoriID = kategoriID;
    }

    public String getBinametrekare() {
        return binametrekare;
    }

    public void setBinametrekare(String binametrekare) {
        this.binametrekare = binametrekare;
    }

    public String getBinafiyat() {
        return binafiyat;
    }

    public void setBinafiyat(String binafiyat) {
        this.binafiyat = binafiyat;
    }

    public String getBinabölge() {
        return binabölge;
    }

    public void setBinabölge(String binabölge) {
        this.binabölge = binabölge;
    }

    public String getBinakredi() {
        return binakredi;
    }

    public void setBinakredi(String binakredi) {
        this.binakredi = binakredi;
    }

    public String getBinayasi() {
        return binayasi;
    }

    public void setBinayasi(String binayasi) {
        this.binayasi = binayasi;
    }

    public String getBinaadres() {
        return binaadres;
    }

    public void setBinaadres(String binaadres) {
        this.binaadres = binaadres;
    }

    public String getBinaaciklama() {
        return binaaciklama;
    }

    public void setBinaaciklama(String binaaciklama) {
        this.binaaciklama = binaaciklama;
    }

    public String getBinasatilikmi() {
        return binasatilikmi;
    }

    public void setBinasatilikmi(String binasatilikmi) {
        this.binasatilikmi = binasatilikmi;
    }

    public String getBinakategoriID() {
        return binakategoriID;
    }

    public void setBinakategoriID(String binakategoriID) {
        this.binakategoriID = binakategoriID;
    }

    public List<mulk> getMulklistesi2() {
        return mulklistesi2;
    }

    public void setMulklistesi2(List<mulk> mulklistesi2) {
        this.mulklistesi2 = mulklistesi2;
    }

    public List<mulk> getMulklistesiAra() {
        return mulklistesiAra;
    }

    public void setMulklistesiAra(List<mulk> mulklistesiAra) {
        this.mulklistesiAra = mulklistesiAra;
    }

    public String getMailIcerik() {
        return mailIcerik;
    }

    public void setMailIcerik(String mailIcerik) {
        this.mailIcerik = mailIcerik;
    }

    public String getMailBaslik() {
        return mailBaslik;
    }

    public void setMailBaslik(String mailBaslik) {
        this.mailBaslik = mailBaslik;
    }

    public String getKayitEmail() {
        return kayitEmail;
    }

    public void setKayitEmail(String kayitEmail) {
        this.kayitEmail = kayitEmail;
    }

    public String getSolAdsoyad() {
        return solAdsoyad;
    }

    public void setSolAdsoyad(String solAdsoyad) {
        this.solAdsoyad = solAdsoyad;
    }

    public String getSolTelno() {
        return solTelno;
    }

    public void setSolTelno(String solTelno) {
        this.solTelno = solTelno;
    }

    public String getSolEmail() {
        return solEmail;
    }

    public void setSolEmail(String solEmail) {
        this.solEmail = solEmail;
    }

    public String getNeredeyimString() {
        return neredeyimString;
    }

    public void setNeredeyimString(String neredeyimString) {
        this.neredeyimString = neredeyimString;
    }

    public UploadedFile getYüklenecekDosya() {
        return yüklenecekDosya;
    }

    public void setYüklenecekDosya(UploadedFile yüklenecekDosya) {
        this.yüklenecekDosya = yüklenecekDosya;
    }
    
    public StreamedContent getDbImage() {
        return dbImage;
    }

    public void setDbImage(StreamedContent dbImage) {
        this.dbImage = dbImage;
    }

    public UploadedFile getYüklenecekDosya2() {
        return yüklenecekDosya2;
    }

    public void setYüklenecekDosya2(UploadedFile yüklenecekDosya2) {
        this.yüklenecekDosya2 = yüklenecekDosya2;
    }
}