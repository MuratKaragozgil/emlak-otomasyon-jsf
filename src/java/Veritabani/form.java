
package Veritabani;

import java.sql.Date;

/**
 *
 * @author OmerFaruk
 */
public class form {
    private String adSoyad,mailAdresi,Telno;
    private Date basvuruTarihi;
    
    form(String ad,String mail,String no,Date tarih)
    {
        adSoyad=ad;
        mailAdresi=mail;
        Telno=no;
        basvuruTarihi=tarih;
    }

    public String getAdSoyad() {
        return adSoyad;
    }

    public void setAdSoyad(String adSoyad) {
        this.adSoyad = adSoyad;
    }

    public String getMailAdresi() {
        return mailAdresi;
    }

    public void setMailAdresi(String mailAdresi) {
        this.mailAdresi = mailAdresi;
    }

    public String getTelno() {
        return Telno;
    }

    public void setTelno(String Telno) {
        this.Telno = Telno;
    }

    public Date getBasvuruTarihi() {
        return basvuruTarihi;
    }

    public void setBasvuruTarihi(Date basvuruTarihi) {
        this.basvuruTarihi = basvuruTarihi;
    }
    
    
}
