/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veritabani;

import static Veritabani.Database.close;
import static Veritabani.Database.getConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Murat
 */
@ManagedBean
@RequestScoped
public class butunTablolar {
        Connection con;
 private List <mulk> arsalistesi;

   
        private List <mulk> dairelistesi;
        private List <mulk> villalistesi;
        private List <mulk> dukkanlistesi;
        private List <mulk> fabrikalistesi;
        private List <mulk> mulklistesi;   
        private List <mulk> kullaniciHesabiListesi;
        private List <mulk> basvuruFormuListesi;
        private List <mulk> maillogListesi;
        private List <mulk> resimTablosuListesi;
   
        private ResultSet rs=null;
        
   public void butunTablolarıGoster(){
         mulklistesi = new ArrayList<>();
        arsalistesi = new ArrayList<>();
        dairelistesi = new ArrayList<>();
        villalistesi = new ArrayList<>();
        dukkanlistesi = new ArrayList<>();
        fabrikalistesi = new ArrayList<>();
        kullaniciHesabiListesi = new ArrayList<>();
        basvuruFormuListesi = new ArrayList<>();
        maillogListesi = new ArrayList<>();
       resimTablosuListesi = new ArrayList<>();
        con = Database.getConnection();
       
       try{
            PreparedStatement SQLKomut = con.prepareStatement("select * from mulk");
           
             rs=SQLKomut.executeQuery(); 
             
             while(rs.next()){
                 
                 mulklistesi.add(new mulk(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),rs.getInt(9)));
                 
                    
             }
             
             SQLKomut = con.prepareStatement("select * from arsadetaylar");
           
             rs=SQLKomut.executeQuery(); 
             
              while(rs.next()){
                 
                 arsalistesi.add(new mulk(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getInt(11),rs.getInt(12)));
                 
                    
             }
             
             
             SQLKomut = con.prepareStatement("select * from dairedetaylar");
           
             rs=SQLKomut.executeQuery(); 
             
             while(rs.next())
             {
                 
                 dairelistesi.add(new mulk(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4),rs.getString(5),rs.getString(6),rs.getInt(7)));
                 
                 
             }
             SQLKomut = con.prepareStatement("select * from dukkandetaylar");
           
             rs=SQLKomut.executeQuery(); 
             
             while(rs.next())
             {
                 
                 dukkanlistesi.add(new mulk(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4),rs.getString(5),rs.getString(6),rs.getInt(7)));
                 
                 
             }
             
              SQLKomut = con.prepareStatement("select * from fabrikadetaylar");
           
             rs=SQLKomut.executeQuery(); 
             
             while(rs.next())
             {
                 
                 fabrikalistesi.add(new mulk(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4),rs.getString(5),rs.getString(6),rs.getInt(7)));
                 
                 
             }
             
             SQLKomut = con.prepareStatement("select * from villadetaylar");
           
             rs=SQLKomut.executeQuery(); 
             
             while(rs.next())
             {
                 
                 villalistesi.add(new mulk(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4),rs.getString(5),rs.getString(6),rs.getInt(7)));
                 
                 
             }
             
             SQLKomut = con.prepareStatement(" select * from kullanicihesabi ");
           
             rs=SQLKomut.executeQuery(); 
             
             while(rs.next())
             {
                 
                 kullaniciHesabiListesi.add(new mulk(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getDate(8)));
                 
                 
             }
             
             
              SQLKomut = con.prepareStatement(" select * from basvuruformu ");
           
             rs=SQLKomut.executeQuery(); 
             
             while(rs.next())
             {
                 
                 basvuruFormuListesi.add(new mulk(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getDate(5)));
                 
                 
             }
             SQLKomut = con.prepareStatement(" select * from maillog");
           
             rs=SQLKomut.executeQuery(); 
             
             while(rs.next())
             {
                 
                 maillogListesi.add(new mulk(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4),rs.getDate(5)));
                 
                 
             }
             
             
               SQLKomut = con.prepareStatement(" select * from resimler ");
           
             rs=SQLKomut.executeQuery(); 
             
             while(rs.next())
             {
                 
                 resimTablosuListesi.add(new mulk(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getBlob(4)));
                 
                 
             }
          }catch(Exception e){  
         }
          
          finally
          {
          
              close(con);
          }
       
       
       
       
       
   }

    public List<mulk> getMulklistesi() {
        return mulklistesi;
    }

    public void setMulklistesi(List<mulk> mulklistesi) {
        this.mulklistesi = mulklistesi;
    }
    public List<mulk> getArsalistesi() {
        return arsalistesi;
    }

    public void setArsalistesi(List<mulk> arsalistesi) {
        this.arsalistesi = arsalistesi;
    }

    public List<mulk> getDairelistesi() {
        return dairelistesi;
    }

    public void setDairelistesi(List<mulk> dairelistesi) {
        this.dairelistesi = dairelistesi;
    }

    public List<mulk> getDukkanlistesi() {
        return dukkanlistesi;
    }

    public void setDukkanlistesi(List<mulk> dukkanlistesi) {
        this.dukkanlistesi = dukkanlistesi;
    }

    public List<mulk> getFabrikalistesi() {
        return fabrikalistesi;
    }

    public void setFabrikalistesi(List<mulk> fabrikalistesi) {
        this.fabrikalistesi = fabrikalistesi;
    }

    public List<mulk> getVillalistesi() {
        return villalistesi;
    }

    public void setVillalistesi(List<mulk> villalistesi) {
        this.villalistesi = villalistesi;
    }

    public List<mulk> getKullaniciHesabiListesi() {
        return kullaniciHesabiListesi;
    }

    public void setKullaniciHesabiListesi(List<mulk> kullaniciHesabiListesi) {
        this.kullaniciHesabiListesi = kullaniciHesabiListesi;
    }
    
     public List<mulk> getBasvuruFormuListesi() {
        return basvuruFormuListesi;
    }

    public void setBasvuruFormuListesi(List<mulk> basvuruFormuListesi) {
        this.basvuruFormuListesi = basvuruFormuListesi;
    }

    public List<mulk> getMaillogListesi() {
        return maillogListesi;
    }

    public void setMaillogListesi(List<mulk> maillogListesi) {
        this.maillogListesi = maillogListesi;
    }

    public List<mulk> getResimTablosuListesi() {
        return resimTablosuListesi;
    }

    public void setResimTablosuListesi(List<mulk> resimTablosuListesi) {
        this.resimTablosuListesi = resimTablosuListesi;
    }
    
    
}
