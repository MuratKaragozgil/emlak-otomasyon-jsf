package Veritabani;



import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import org.primefaces.context.RequestContext;

@SessionScoped
@ManagedBean(name="db")
public class VeriTabanıBağlantı extends SqlKomutları
{
DataSource dataSource;
Connection con=null;

        public String login(){
            boolean result = login(this.getUserName(),this.getPassWord());
          
          if (result) {
            
            if(getAdminmi().equalsIgnoreCase("e"))
                return "adminPaneli.xhtml?faces-redirect=true";
            else
                return "kullaniciPaneli.xhtml?faces-redirect=true";
               } 
        else {
               FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mesaj", "Kullanıcı Adı ve ya Şifre Hatalı!");
               RequestContext.getCurrentInstance().showMessageInDialog(message);
            return "index.xhtml?hataliGiris=1faces-redirect=true";
        }
        }   
        
        public String logout()
        {
            setkAdi(null);
            setkSifre(null);
            setkId(null);
               return "index.xhtml?faces-redirect=true";
        }
}
