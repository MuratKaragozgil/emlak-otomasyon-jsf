package Veritabani;

import java.sql.Connection;
import java.sql.DriverManager;
public class Database extends User {
    
     public static Connection getConnection() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1;databaseName=EmlakDatabase",
                    "", "");
            return con;
        } 
        catch (Exception ex) {
            System.out.println("Database.getConnection() HATASI" + ex.getMessage());
            return null;
        }
    }
 
    public static void close(Connection con) {
        try {
            con.close();
        } catch (Exception ex) {
        }
    }
}